#!/bin/bash
#Script to choose monitor
DMENU="dmenu -i -c -l 10"
intern=eDP
extern=HDMI-A-0
choice=$( cat /home/aadhi/.local/bin/scripts/script_files/monitor-choices | $DMENU )
if [ $choice == "External" ]; then
	if xrandr | grep "$extern disconnected"; then
		xrandr --output "$extern" --off --output "$intern" --auto 
		nitrogen --restore 
		sleep 1s
	else
    	xrandr --output "$intern" --off --output "$extern" --auto 
		nitrogen --restore 
		sleep 1s
	fi
elif [ $choice == "Both" ]; then
	if xrandr | grep "$extern disconnected"; then
		xrandr --output "$extern" --off --output "$intern" --auto --primary
		nitrogen --restore 
		sleep 1s
	else
		choice2=$(cat /home/aadhi/.local/bin/scripts/script_files/monitor_location | $DMENU -p "Location wrt internal display")
    	xrandr --output "$intern" --auto --output "$extern" --auto --primary $choice2 $intern
		nitrogen --restore 
		sleep 1s
	fi
elif [ $choice == "Internal" ]; then
	xrandr --output "$extern" --off --output "$intern" --auto --primary
	nitrogen --restore 
	sleep 1s
else 
	sleep 1s
fi
	
