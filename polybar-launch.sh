#!/usr/bin/env bash

# Terminate already running bar instances
# If all your bars have ipc enabled, you can use 
#polybar-msg cmd quit
pkill polybar
# Otherwise you can use the nuclear option:
# killall -q polybar

# Launch bar1 and bar2
polybar mainbar0 & disown
polybar mainbar1 & disown

echo "Bars launched..."