#!/bin/bash
intern=eDP
extern=HDMI-A-0

	if xrandr | grep "$extern disconnected"; then
		xrandr --output "$extern" --off --output "$intern" --auto 
		nitrogen --restore 
		sleep 1s
	else
		xrandr --output "$intern" --auto --output "$extern" --auto --primary --right-of $intern
		nitrogen --restore 
		sleep 1s
	fi
